#!/usr/bin/env bash

get_os(){
    if [ "$(uname)" == "Darwin" ]; then
    echo "OSX"   
    elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    echo "LINUX"
    elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
    echo "MINGW32_NT"
    elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
    echo "MINGW64_NT"
    fi
}

apt-get update
apt-get install -y software-properties-common wget git

add-apt-repository -y ppa:ubuntu-toolchain-r/test

cat << EOF > /etc/apt/sources.list.d/llvm-toolchain.list
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty main
# 3.4
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.4 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.4 main
# 3.5
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.5 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.5 main
# 3.6
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.6 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.6 main
# 3.7
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.7 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.7 main
# 3.8
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.8 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.8 main
# 3.9
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.9 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-3.9 main
# 5 
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-5.0 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-5.0 main
# 6 
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-6.0 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-6.0 main
# 7 
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-7 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-7 main
# 8 
deb http://apt.llvm.org/trusty/ llvm-toolchain-trusty-8 main
deb-src http://apt.llvm.org/trusty/ llvm-toolchain-trusty-8 main
# Also add the following for the appropriate libstdc++
# deb http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu trusty main
EOF

wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -

apt-get update
apt-get upgrade -y

install() {
    for i in $@; do
        echo Installing $i
        apt-get install -y $i
    done
}

programs=(lcov valgrind cmake3 libyajl-dev libxml2-dev libxqilla-dev libssl-dev libcurl4-openssl-dev libgtest-dev libstdc++-5-dev)

install ${programs[@]}