FROM ubuntu:trusty

WORKDIR /var/workdir
COPY tools/scripts/cpp-toolchain.bash /usr/local/bin
RUN cpp-toolchain.bash && rm -rf var/lib/apt/lists